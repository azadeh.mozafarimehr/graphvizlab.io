---
name: TK graphics
params:
- tk
---
Output using the text-based TK graphics primitives.
