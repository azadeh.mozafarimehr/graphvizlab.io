---
defaults:
- '1.0'
flags: []
minimums:
- '0.0'
name: arrowsize
types:
- double
used_by: E
---
Multiplicative scale factor for arrowheads.
