---
defaults:
- black
flags: []
minimums: []
name: fontcolor
types:
- color
used_by: ENGC
---
Color used for text.
