---
defaults:
- '""'
flags:
- map
- svg
minimums: []
name: edgehref
types:
- escString
used_by: E
---
Synonym for [`edgeURL`](#d:edgeURL).
