---
defaults:
- node
flags: []
minimums: []
name: packmode
types:
- packMode
used_by: G
---
This indicates how connected components should be packed (cf.
[`packMode`](#k:packMode)). Note that defining `packmode` will automatically
turn on packing as though one had set `pack=true`.
