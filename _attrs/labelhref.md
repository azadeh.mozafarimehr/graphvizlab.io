---
defaults:
- '""'
flags:
- map
- svg
minimums: []
name: labelhref
types:
- escString
used_by: E
---
Synonym for [`labelURL`](#d:labelURL).
